﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [EventStatus] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_EventStatus] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [ListPlanned] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_ListPlanned] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [EventPlanned] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NOT NULL,
    [Description] nvarchar(max) NULL,
    [DateTodo] datetime2 NULL,
    [EventStatusId] int NOT NULL DEFAULT (((1))),
    [ListPlannedId] int NULL DEFAULT (((null))),
    CONSTRAINT [PK_EventPlanned] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_EventPlanned_EventStatus_EventStatusId] FOREIGN KEY ([EventStatusId]) REFERENCES [EventStatus] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_EventPlanned_ListPlanned_ListPlannedId] FOREIGN KEY ([ListPlannedId]) REFERENCES [ListPlanned] ([Id]) ON DELETE SET NULL
);

GO

CREATE INDEX [IX_EventPlanned_EventStatusId] ON [EventPlanned] ([EventStatusId]);

GO

CREATE INDEX [IX_EventPlanned_ListPlannedId] ON [EventPlanned] ([ListPlannedId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190411133903_init', N'2.2.2-servicing-10034');

GO

