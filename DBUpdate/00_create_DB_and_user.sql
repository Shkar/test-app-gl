﻿DECLARE
  @DatabaseName AS NVARCHAR(128),
  @Username AS NVARCHAR(128),
  @Password AS NVARCHAR(128),
  @SQL AS NVARCHAR(MAX)

SELECT
  @DatabaseName = 'MyPlannerDB',
  @Username = 'MyPlannerDBUser',
  @Password = 'Passw0rd'

SET @SQL = 'CREATE DATABASE [' + @DatabaseName + ']'
EXEC (@SQL)

SET @SQL = 'CREATE LOGIN [' + @Username + '] WITH PASSWORD = ''' + @Password + ''''
EXEC (@SQL)

SET @SQL = 'USE ' + @DatabaseName
EXEC (@SQL)

SET @SQL = 'CREATE USER [' + @Username + '] FOR LOGIN [' + @Username + ']'
EXEC (@SQL)

EXEC sp_addrolemember 'db_owner', @username

