﻿INSERT INTO [dbo].[EventStatus] ([Name]) VALUES  ('New')
GO

INSERT INTO [dbo].[EventStatus] ([Name]) VALUES  ('In Progress')
GO

INSERT INTO [dbo].[EventStatus] ([Name]) VALUES  ('Done')
GO

INSERT INTO [dbo].[EventStatus] ([Name]) VALUES  ('Canceled')
GO
