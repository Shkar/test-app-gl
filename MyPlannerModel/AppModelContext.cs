﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using System.Configuration;


namespace MyPlannerModel
{
    public partial class AppModelContext : DbContext
    {
        public AppModelContext()
        {
        }

        public AppModelContext(DbContextOptions options)
            : base(options)
        {
        }

        public virtual DbSet<EventPlanned> EventPlanned { get; set; }
        public virtual DbSet<EventStatus> EventStatus { get; set; }
        public virtual DbSet<ListPlanned> ListPlanned { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["MyAppConnectionString"].ConnectionString)
                //optionsBuilder.UseSqlServer("Server=WS-LV-CP2435;Database=MyPlannerDB;User Id=MyPlannerDBUser;Password=Passw0rd;")
                    .UseLoggerFactory(logger);

                var lf = new LoggerFactory();
                lf.AddProvider(new FileLoggerProvider());
                optionsBuilder.UseLoggerFactory(lf)
                    .EnableSensitiveDataLogging(true);
            }
        }

        public static readonly LoggerFactory logger = new LoggerFactory(new[]{new ConsoleLoggerProvider
        ((category, level)=> category == DbLoggerCategory.Database.Command.Name
                             && level == LogLevel.Information, true), });

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.3-servicing-35854");

            modelBuilder.Entity<EventPlanned>(entity =>
            {
                entity.HasIndex(e => e.EventStatusId);

                entity.HasIndex(e => e.ListPlannedId);

                entity.Property(e => e.EventStatusId).HasDefaultValueSql("((1))");

                entity.Property(e => e.ListPlannedId).HasDefaultValueSql("((null))");

                entity.Property(e => e.ListPlannedId).ValueGeneratedOnAdd();

                entity.Property(e => e.Name).IsRequired();

                entity.HasOne(d => d.EventStatus)
                    .WithMany(p => p.EventPlanned)
                    .HasForeignKey(d => d.EventStatusId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.Property(e => e.ListPlannedId).IsRequired(false);

                entity.HasOne(d => d.ListPlanned)
                    .WithMany(p => p.EventPlanned)
                    .HasForeignKey(d => d.ListPlannedId).IsRequired(false)
                    .OnDelete(DeleteBehavior.Restrict)
                    ;
            });

            modelBuilder.Entity<EventStatus>(entity =>
            {
                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<ListPlanned>(entity =>
            {
                entity.Property(e => e.Name).IsRequired();
                entity.HasMany(e => e.EventPlanned)
                    .WithOne(l => l.ListPlanned)
                    .OnDelete(DeleteBehavior.SetNull);
            });
        }
    }
}
