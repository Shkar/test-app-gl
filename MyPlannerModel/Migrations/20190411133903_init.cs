﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyPlannerModel.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EventStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ListPlanned",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ListPlanned", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EventPlanned",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DateTodo = table.Column<DateTime>(nullable: true),
                    EventStatusId = table.Column<int>(nullable: false, defaultValueSql: "((1))"),
                    ListPlannedId = table.Column<int>(nullable: true, defaultValueSql: "((null))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventPlanned", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EventPlanned_EventStatus_EventStatusId",
                        column: x => x.EventStatusId,
                        principalTable: "EventStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EventPlanned_ListPlanned_ListPlannedId",
                        column: x => x.ListPlannedId,
                        principalTable: "ListPlanned",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EventPlanned_EventStatusId",
                table: "EventPlanned",
                column: "EventStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_EventPlanned_ListPlannedId",
                table: "EventPlanned",
                column: "ListPlannedId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EventPlanned");

            migrationBuilder.DropTable(
                name: "EventStatus");

            migrationBuilder.DropTable(
                name: "ListPlanned");
        }
    }
}
