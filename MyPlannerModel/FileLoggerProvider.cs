﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Configuration;
using System.Collections.Specialized;

namespace MyPlannerModel
{
    public class FileLoggerProvider : ILoggerProvider
    {
        public ILogger CreateLogger(string categoryName)
        {
            return new FileLogger();
        }

        public void Dispose()
        { }

        private class FileLogger : ILogger
        {
            public bool IsEnabled(LogLevel logLevel)
            {
                return true;
            }

            public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
            {
                File.AppendAllText(ConfigurationManager.AppSettings.Get("LogFilePath"), formatter(state, exception));
                //Console.WriteLine(formatter(state, exception));
            }

            public IDisposable BeginScope<TState>(TState state)
            {
                return null;
            }
        }
    }
}