﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyPlannerModel;

namespace MyPlannerConsole
{
    interface ICommand
    {
        string Usage();
        string Description();
        int Run(string[] arguments);
    }
    public class CommandsProcessor
    {
        private Dictionary<string, ICommand> _commands;
        public CommandsProcessor()
        {
            var allCommands = new ICommand[]
            {
                new AddEvent(),
                new UpdateEvent(), 
                new DeleteEvent(),
                new SearchEvent(), 
                new Dashboard(),
                new DisplayEventsFromList(),
                new AddList(),
                new UpdateList(),
                new ClearList(), 
                new DeleteList()
            };

            var usage = new UsageCommand(allCommands);

            _commands = new Dictionary<string, ICommand>()
            {
                { AddEvent.Key.ToLower(), new AddEvent()},
                { UpdateEvent.Key.ToLower(), new UpdateEvent()},
                { DeleteEvent.Key.ToLower(), new DeleteEvent()},
                { SearchEvent.Key.ToLower(), new SearchEvent()},
                { Dashboard.Key.ToLower(), new Dashboard()},
                { DisplayEventsFromList.Key.ToLower(), new DisplayEventsFromList()},
                { AddList.Key.ToLower(), new AddList()},
                { UpdateList.Key.ToLower(), new UpdateList()},
                { ClearList.Key.ToLower(), new ClearList()},
                { DeleteList.Key.ToLower(), new DeleteList()},
                { UsageCommand.Key.ToLower(), usage}
            };
        }

        public int Process(string[] args)
        {
            if (args != null && args.Length >= 1)
            {
                return ExecuteCommand(args);
            }
            return GetParamsAndExecuteCommand();
        }

        private int ExecuteCommand(string[] args)
        {
            return RunCommand(string.Join(" ", args));
        }

        private int GetParamsAndExecuteCommand()
        {
            Console.WriteLine("Input command or {0} for help", UsageCommand.Key);
            while (true)
            {
                var input = Console.ReadLine();
                if (string.Equals(input, "exit", StringComparison.InvariantCultureIgnoreCase))
                {
                    return ExitCode.Success;
                }
                    
                if (!string.IsNullOrEmpty(input))
                {
                    RunCommand(input);
                }
            }
        }

        private int RunCommand(string input)
        {
            Console.WriteLine("attempt to run command'{0}'", input);
            var splittedInput = Shell32.CommandLineToArgs(input);
            if (splittedInput.Length > 0)
            {
                var commandText = splittedInput[0].ToLower();
                ICommand command;
                if(_commands.TryGetValue(commandText, out command))
                {
                   var RunCommandWithParams =command.Run(splittedInput.Skip(1).ToArray());
                   switch (RunCommandWithParams)
                   {
                        case ExitCode.Success: Console.WriteLine("Command {0} was executed", commandText);break;
                        case ExitCode.Error: Console.WriteLine("Command {0} was failed", commandText); break;
                        default: throw new ArgumentOutOfRangeException("RunCommandWithParams");
                    }
                   return RunCommandWithParams;
                }
                else
                {
                    Console.WriteLine("Unknown command");
                }
            }
            return ExitCode.Error;
        }
    }
}