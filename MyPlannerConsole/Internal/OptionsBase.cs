﻿using System;
using System.Collections.Generic;
using System.Text;
using CommandLine;
using CommandLine.Text;

namespace MyPlannerConsole
{
    internal class OptionsBase
    {
        [ParserState] public IParserState LastParserState { get; set; }

        public string GetUsage()
        {
            var help = HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
            help.MaximumDisplayWidth = 120;
            help.Copyright = new CopyrightInfo("AI", 2019);
            help.Heading = "\nMy Planner";
            return help;
        }

        public string GetUsageForUsageList()
        {
            var help = HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
            help.Copyright = " ";
            help.Heading = " ";
            return help.ToString().Trim('\r', '\n');
        }
    }
}
