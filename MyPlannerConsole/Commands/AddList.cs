﻿using System;
using CommandLine;
using Methods;
using MyPlannerModel;

namespace MyPlannerConsole
{
    internal class AddListOptions : OptionsBase
    {
        //[Option("Id", Required=true)] public int Id { get; set; }
        [Option('n', "name", Required = true, HelpText = "Name of List. Please enclause name with \" \" if name is not a single word")]
        public string Name { get; set; }
    }

    internal class AddList : ICommand
    {
        public const string Key = "add-list";


        public string Usage()
        {
            return string.Format("{0}{1}", Key, new AddEventOptions().GetUsageForUsageList());
        }

        public string Description()
        { return string.Format("Description: Command \"{0}\" Creates new List in \"My Planner\".", Key); }

        public int Run(string[] arguments)
        {
            var options = new AddListOptions();
            if (OptionsParser.Instance.ParseArguments(arguments, options))
            {
                return Add(options);
            }
                
            return ExitCode.Error;
        }

        private int Add(AddListOptions options)
        {
            try
            {
                ListPlanned listPlanned = new ListPlanned();
                listPlanned.Name = options.Name;
                ListMethods.AddListPlannedDb(listPlanned);
                Console.WriteLine();
                Console.WriteLine("List \"{0}\" was created", options.Name);

                return ExitCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ExitCode.Error;
            }
        }
    }
}

