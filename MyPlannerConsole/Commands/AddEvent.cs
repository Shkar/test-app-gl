﻿using System;
using CommandLine;
using Methods;
using MyPlannerModel;

namespace MyPlannerConsole
{
    internal class AddEventOptions : OptionsBase
    {
        //[Option("Id", Required=true)] public int Id { get; set; }
        [Option('n', "name", Required = true, HelpText = "Name of Event. Please enclause name with \" \" if name is not a single word")]
        public string Name { get; set; }

        [Option('d', "description", Required = false, HelpText = "Description of event")]
        public string Description { get; set; }

        [Option("date", Required = false, HelpText = "Scheduled date to do.")]
        public DateTime? DateToDo { get; set; }

        //[Option('l', "ListId")] public int ListId { get; set; }
    }

    internal class AddEvent : ICommand
        {
            public const string Key = "add-event";


            public string Usage()
            {
                return string.Format("{0}{1}", Key, new AddEventOptions().GetUsageForUsageList());
            }

            public string Description()
            {return string.Format("Description: Command \"{0}\" Creates new Item in \"My Planner\". All items are created with Status \"New\"", Key);}

            public int Run(string[] arguments)
            {
                var options = new AddEventOptions();
                if (OptionsParser.Instance.ParseArguments(arguments, options))
                {
                    return Add(options, new AppModelContext());
                }
  
                return ExitCode.Error;
            }

            private int Add(AddEventOptions options, AppModelContext context)
            {
                try
                {
                    EventPlanned eventPlanned = new EventPlanned();
                    eventPlanned.Name = options.Name;
                    if(options.Description!=null)
                        eventPlanned.Description = options.Description;
                    if(options.DateToDo!=null)
                        eventPlanned.DateTodo = options.DateToDo;
                    new EventMethods(context).AddNewPlannedEventDb(eventPlanned);

                    return ExitCode.Success;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return ExitCode.Error;
                }
            }
        }
    }

