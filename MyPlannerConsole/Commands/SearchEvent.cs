﻿using System;
using System.Collections.Generic;
using CommandLine;
using Methods;
using MyPlannerConsole.Commands;
using MyPlannerModel;

namespace MyPlannerConsole
{
    internal class SearchEventOptions : OptionsBase
    {

        [Option('t', "Text", Required = false, HelpText = "Text that is used to search")]
        public string Text { get; set; }

        [Option('s', "Status", Required = false, HelpText = "Displays all events with selected status")]
        public string Status { get; set; }
    }

    internal class SearchEvent : ICommand
    {
        public const string Key = "Search-event";


        public string Usage()
        {
            return string.Format("{0}{1}", Key, new SearchEventOptions().GetUsageForUsageList());
        }

        public string Description()
        { return string.Format("Description: Command \"{0}\" searches planed events by Event name or Event Description.", Key); }

        public int Run(string[] arguments)
        {
            var options = new SearchEventOptions();
            if (OptionsParser.Instance.ParseArguments(arguments, options))
            {
                return Search(options, new AppModelContext());
            }
                
            return ExitCode.Error;
        }

        private int Search(SearchEventOptions options, AppModelContext context)
        {
            try
            {
                if (options.Text != null && options.Status != null)
                {
                    Console.WriteLine("Please use only one argument for search");
                    return ExitCode.Success;
                }

                if (options.Text!=null)
                {
                    var eventitem = new EventMethods(context).GetPlannedEventDb(options.Text);
                    new DisplayDetailsBase().DisplayDetails(eventitem, string.Format("search by text \"{0}\"", options.Text));
                }


                if (options.Status!=null)
                {
                    var eventStatus = Int32.TryParse(options.Status, out int statusId)
                        ? StatusMethods.GetEventStatusDb(statusId) : StatusMethods.GetEventStatusDb(options.Status);

                    var eventitem = new EventMethods(context).GetPlannedEventDb(eventStatus);
                    new DisplayDetailsBase().DisplayDetails(eventitem, string.Format("search by Status \"{0}\"", eventStatus.Name));
                }

                return ExitCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ExitCode.Error;
            }
        }
    }
}

