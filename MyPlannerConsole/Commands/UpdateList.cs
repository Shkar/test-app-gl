﻿using System;
using CommandLine;
using Methods;
using MyPlannerModel;

namespace MyPlannerConsole
{
    internal class UpdateListOptions : OptionsBase
    {
        [Option('i', "Id", Required=true, HelpText = "If of list that should be modified")] public int Id { get; set; }
        [Option('n', "name", Required = false, HelpText = "Name of List. Please enclause name with \" \" if name is not a single word")]
        public string Name { get; set; }
    }

    internal class UpdateList : ICommand
    {
        public const string Key = "update-list";


        public string Usage()
        {
            return string.Format("{0}{1}", Key, new UpdateListOptions().GetUsageForUsageList());
        }

        public string Description()
        { return string.Format("Description: Command \"{0}\" Updates existing List in \"My Planner\".", Key); }

        public int Run(string[] arguments)
        {
            var options = new UpdateListOptions();
            if (OptionsParser.Instance.ParseArguments(arguments, options))
            {
                return Update(options);
            }
            return ExitCode.Error;
        }

        private int Update(UpdateListOptions options)
        {
            try
            {
                ListPlanned listPlanned = ListMethods.GetListPlannedDb(options.Id);
                if (listPlanned != null)
                {
                    if (options.Name != null)
                    {
                        listPlanned.Name = options.Name;
                        ListMethods.UpdateListPlannedDb(listPlanned);
                        Console.WriteLine("List with Id {0} was updated", listPlanned.Id);
                    }
                    else
                    {
                        Console.WriteLine("Please provide any value for modification");
                    }
                }

                return ExitCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ExitCode.Error;
            }
        }
    }
}

