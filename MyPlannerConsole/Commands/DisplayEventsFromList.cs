﻿using System;
using System.Collections.Generic;
using CommandLine;
using Methods;
using MyPlannerConsole.Commands;
using MyPlannerModel;

namespace MyPlannerConsole
{
    internal class DisplayEventsFromListOptions : OptionsBase
    {
        [Option('l', "list", Required = true, HelpText = "List name or Id")]
        public string text { get; set; }
    }

    internal class DisplayEventsFromList : ICommand
    {
        public const string Key = "list-events";
        
        public string Usage()
        {
            return string.Format("{0}{1}", Key, new DisplayEventsFromListOptions().GetUsageForUsageList());
        }

        public string Description()
        { return string.Format("Description: Command \"{0}\" Shows all events from provided list.", Key); }

        public int Run(string[] arguments)
        {
            var options = new DisplayEventsFromListOptions();
            if (OptionsParser.Instance.ParseArguments(arguments, options))
            {
                return DisplayEvents(options, new AppModelContext());
            }
                
            return ExitCode.Error;
        }

        private int DisplayEvents(DisplayEventsFromListOptions options, AppModelContext context)
        {
            try
            {
                ListPlanned listPlanned;
                int listPlannedId;
                if (Int32.TryParse(options.text, out listPlannedId))
                {
                    listPlannedId = int.Parse(options.text);
                    listPlanned = ListMethods.GetListPlannedDb(listPlannedId);
                }
                else
                {
                    listPlanned = ListMethods.GetListPlannedDb(options.text);
                }

                if(listPlanned!=null)
                {
                    var eventitem = new EventMethods(context).GetPlannedEventDb(listPlanned);
                    new DisplayDetailsBase().DisplayDetails(eventitem, string.Format("List \"{0}\"", listPlanned.Name));
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("List not found");
                    Console.WriteLine();
                }
                return ExitCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ExitCode.Error;
            }
        }
    }
}

