﻿using System;
using Methods;
using MyPlannerConsole.Commands;
using MyPlannerModel;

namespace MyPlannerConsole
{


    internal class Dashboard : ICommand
    {
        public const string Key = "Dashboard";


        public string Usage()
        {
            return string.Format("{0}", Key);
        }

        public string Description()
        { return string.Format("Description: Command \"{0}\" shows all items planned for current date \"My Planner\".", Key); }

        public int Run(string[] arguments)
        {
            return ShowDashboard(new AppModelContext());
        }

        private int ShowDashboard(AppModelContext context) 
        {
            try
            {
                var dashboardEvents = new EventMethods(context).GetPlannedEventDb(DateTime.Today);
                new DisplayDetailsBase().DisplayDetails(dashboardEvents, 
                    string.Format( "Current date {0}", DateTime.Today.ToShortDateString()));
                return ExitCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ExitCode.Error;
            }
        }
    }
}

