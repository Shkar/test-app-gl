﻿using System;
using CommandLine;
using Methods;
using MyPlannerModel;

namespace MyPlannerConsole
{
    internal class ClearListOptions : OptionsBase
    {
        [Option('i', "Id", Required = true, HelpText = "If of list that should be modified")] public int Id { get; set; }
    }

    internal class ClearList : ICommand
    {
        public const string Key = "Clear-list";


        public string Usage()
        {
            return string.Format("{0}{1}", Key, new ClearListOptions().GetUsageForUsageList());
        }

        public string Description()
        { return string.Format("Description: Command \"{0}\" Updates existing List in \"My Planner\".", Key); }

        public int Run(string[] arguments)
        {
            var options = new ClearListOptions();
            if (OptionsParser.Instance.ParseArguments(arguments, options))
            {
                return Clear(options, new AppModelContext());
            }
            return ExitCode.Error;
        }

        private int Clear(ClearListOptions options, AppModelContext context)
        {
            try
            {
                ListPlanned listPlanned = ListMethods.GetListPlannedDb(options.Id);
                if (listPlanned != null)
                {
                    foreach (var eventItem in listPlanned.EventPlanned)
                    {
                        eventItem.ListPlanned = null;
                        eventItem.ListPlannedId = null;
                        new EventMethods(context).UpdatePlannedEventDb(eventItem);
                    }
                }

                return ExitCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ExitCode.Error;
            }
        }
    }
}

