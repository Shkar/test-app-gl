using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using MyPlannerModel;
using Methods;


namespace UnitTest
{
    public class ModelTests
    {
        [Fact]
        public void CheckNameValid()
        {
            EventPlanned eventPlanned = new EventPlanned { Name = "test name" };
            Assert.Equal("test name", eventPlanned.Name);
        }

        [Fact]
        public void CheckNameException()
        {
            EventPlanned eventPlanned = new EventPlanned();
            Assert.Throws<ArgumentException>(() => eventPlanned.Name = new string('a', 20));
        }



        [Fact]
        public void CheckDate()
        {
            EventPlanned eventPlanned = new EventPlanned { Name = "test name", DateTodo = new DateTime(2011, 1, 1) };
            Assert.IsType<DateTime>(eventPlanned.DateTodo);
            Assert.Equal(new DateTime(2011, 1, 1), eventPlanned.DateTodo);

        }

        [Fact]
        public void checkMoq()
        {
            //https://docs.microsoft.com/en-us/ef/ef6/fundamentals/testing/mocking
            EventPlanned eventPlanned = new EventPlanned { Id = 1, Name = "test name", DateTodo = new DateTime(2011, 1, 1) };
            var fakeList = new List<EventPlanned>();

            var mockContext = new Mock<AppModelContext>();
            var mockSet = new Mock<DbSet<EventPlanned>>();

            //mockContext.Setup(x => x.EventPlanned.Add(It.IsAny<EventPlanned>())).Returns((EventPlanned e) => e);
            mockContext.Setup(x => x.EventPlanned).Returns(mockSet.Object);



            var service = new EventMethods(mockContext.Object);
            //service.AddNewPlannedEventDb(eventPlanned);

            mockContext.Object.EventPlanned.Add(eventPlanned);

            var result = service.GetPlannedEventDb(1);
            //mockContext.Verify(m => m.SaveChanges(), Times.Once());
            Assert.Equal("test name", result.Name);
        }

        public AppModelContext GetContext()
        {
            var options = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            return new AppModelContext(options);
        }

        [Fact]
        public void TestMultiple()
        {
            //https://github.com/aspnet/EntityFrameworkCore/issues/8316
            var context = GetContext();
            var service = new EventMethods(context);

            var events = new[]
                {
                    new EventPlanned {Id = 1, Name = "test name", DateTodo = new DateTime(2011, 1, 1) },
                    new EventPlanned {Id = 2, Name = "test test", DateTodo = new DateTime(2011, 1, 1) }
                 };

            context.EventPlanned.AddRange(events);
            context.SaveChanges();

            var result = service.GetPlannedEventDb(2);

            Assert.Equal("test test", result.Name);
        }

        [Fact]
        public void Check_Event_Name()
        {
            //https://github.com/aspnet/EntityFrameworkCore/issues/8316
            var context = GetContext();
            var service = new EventMethods(context);

            var events = new EventPlanned {Id = 1, Name = "test name", DateTodo = new DateTime(2011, 1, 1)};
                

            context.EventPlanned.AddRange(events);
            context.SaveChanges();

            var result = service.GetPlannedEventDb(1);

            Assert.Equal("test name", result.Name);
        }

        [Fact]
        public void Check_Event_Date()
        {
            //https://github.com/aspnet/EntityFrameworkCore/issues/8316
            var context = GetContext();
            var service = new EventMethods(context);

            var events = new EventPlanned { Id = 1, Name = "test name", DateTodo = new DateTime(2011, 1, 1) };


            context.EventPlanned.AddRange(events);
            context.SaveChanges();

            var result = service.GetPlannedEventDb(1);

            Assert.IsType<DateTime>(result.DateTodo);
            Assert.Equal(new DateTime(2011, 1, 1), result.DateTodo);
        }

        [Fact(Skip = "icluded entites are not supported")]
        public void Check_Event_Status_Id()
        {
            var context = GetContext();
            var service = new EventMethods(context);

            var events = new EventPlanned { Id = 1, Name = "test name", DateTodo = new DateTime(2011, 1, 1) };


            context.EventPlanned.AddRange(events);
            context.SaveChanges();

            var result = service.GetPlannedEventDb(1);
           
            Assert.Equal(1, result.EventStatusId);
        }

        [Fact (Skip = "icluded entites are not supported")]
        public void Check_Event_Status_Name()
        {
            var context = GetContext();
            var service = new EventMethods(context);

            var events = new EventPlanned { Id = 1, Name = "test name", DateTodo = new DateTime(2011, 1, 1) };


            context.EventPlanned.AddRange(events);
            context.SaveChanges();

            var result = service.GetPlannedEventDb(1);

            Assert.Equal("New", result.EventStatus.Name);
        }

        [Fact]
        public void Search_By_Name()
        {
            //https://github.com/aspnet/EntityFrameworkCore/issues/8316
            var context = GetContext();
            var service = new EventMethods(context);

            var events = new[]
            {
                new EventPlanned {Id = 1, Name = "test name", DateTodo = new DateTime(2011, 1, 1) },
                new EventPlanned {Id = 2, Name = "test test", DateTodo = new DateTime(2011, 1, 1) }
            };

            context.EventPlanned.AddRange(events);
            context.SaveChanges();

            var result = service.GetPlannedEventDb("name");

            Assert.Single(result);
            Assert.Equal("test name", result[0].Name);
        }
    }
}
