﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MyPlannerModel;

namespace Methods
{
    public class EventMethods
    {

        private readonly AppModelContext _appContext;

        public EventMethods (AppModelContext appContext)
        {
            _appContext = appContext;
        }

        public EventPlanned GetPlannedEventDb(int id)
        {
                return _appContext.EventPlanned
                    //.Include(e=>e.EventStatus)
                    //.Include(e => e.ListPlanned)
                    .AsNoTracking()
                    .SingleOrDefault(e => e.Id == id);
        }

        public List<EventPlanned> GetPlannedEventDb(ListPlanned listPlanned)
        {
                return _appContext.EventPlanned
                    .Where(e => e.ListPlanned == listPlanned)
                    .Include(e => e.EventStatus)
                    .AsNoTracking()
                    .ToList();

        }

        public  List<EventPlanned> GetPlannedEventDb()
        {
            return _appContext.EventPlanned
                .Include(e => e.EventStatus)
                .Include(e => e.ListPlanned)
                .AsNoTracking()
                .ToList();
        }
   
        public List<EventPlanned> GetPlannedEventDb(string text)
        {
            return _appContext.EventPlanned
                    .Where(e =>e.Name.Contains(text) || e.Description.Contains(text))
                    .Include(e => e.ListPlanned)
                    .Include(e => e.EventStatus)
                    .AsNoTracking()
                    .ToList();
        }

        public  List<EventPlanned> GetPlannedEventDb(DateTime date)
        {
            return _appContext.EventPlanned
                    .Where(e => e.DateTodo == date)
                    .Include(e => e.EventStatus)
                    .Include(e => e.ListPlanned)
                    .AsNoTracking()
                    .ToList();
        }

        public  List<EventPlanned> GetPlannedEventDb(EventStatus eventStatus)
        {
                return _appContext.EventPlanned
                    .Where(e => e.EventStatus == eventStatus)
                    .Include(e => e.EventStatus)
                    .Include(e => e.ListPlanned)
                    .AsNoTracking()
                    .ToList();
        }

        public  void DeletePlannedEventByIdDb(int id)
        {
                var eventToDelete = _appContext.EventPlanned.Find(id);
                if (eventToDelete!=null)
                {
                    _appContext.EventPlanned.Remove(_appContext.EventPlanned.Find(id));
                    _appContext.SaveChanges();
                }
        }

        public void AddNewPlannedEventDb(EventPlanned eventToAdd)
        {
            _appContext.EventPlanned.Add(eventToAdd);
            _appContext.SaveChanges();
        }

        public  void UpdatePlannedEventDb(EventPlanned eventToEdit)
        {
            _appContext.EventPlanned.Update(eventToEdit);
            _appContext.SaveChanges();
        }
    }
}
